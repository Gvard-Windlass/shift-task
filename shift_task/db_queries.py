from datetime import date

from sqlalchemy.orm import Session

from . import models, schemas


def get_employee(db: Session, login: str):
    return db.query(models.Empolyee).filter(models.Empolyee.login == login).first()


def get_employee_position_info(db: Session, employee_id: int):
    salary = (
        db.query(models.Position.salary)
        .join(models.Promotion)
        .filter(
            models.Promotion.employee_id == employee_id,
            models.Promotion.promotion_date <= date.today(),
        )
        .order_by(models.Promotion.promotion_date.desc())
        .limit(1)
        .scalar()
    )

    next_promotion_date = (
        db.query(models.Promotion.promotion_date)
        .filter(
            models.Promotion.employee_id == employee_id,
            models.Promotion.promotion_date > date.today(),
        )
        .order_by(models.Promotion.promotion_date.asc())
        .limit(1)
        .scalar()
    )

    return schemas.PositionInfo(salary=salary, next_promotion_date=next_promotion_date)
