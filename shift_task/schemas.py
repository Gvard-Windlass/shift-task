from datetime import date
from pydantic import BaseModel


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    username: str | None = None


class PositionInfo(BaseModel):
    salary: float | None
    next_promotion_date: date | None


class Employee(BaseModel):
    id: int
    login: str
    first_name: str
    last_name: str
    surname: str
