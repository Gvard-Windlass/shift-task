import uvicorn
from datetime import timedelta, datetime
from typing import Annotated

from fastapi import Depends, FastAPI, HTTPException, status
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from sqlalchemy.orm import Session
from jose import JWTError, jwt, ExpiredSignatureError

from .db_config import SessionLocal
from .db_queries import get_employee, get_employee_position_info
from .schemas import PositionInfo, Token, TokenData, Employee
from .utils import verify_password

SECRET_KEY = "345b4afbdd255d1f40ffc0dcc0f5d57d687ab025760fc637baf9e5c2ef45ed8b"
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 30

app = FastAPI()

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


def authenticate_user(db: Session, login: str, password: str):
    user = get_employee(db, login)
    if not user:
        return False
    if not verify_password(password, user.hashed_password):
        return False
    return user


class TokenCreate:
    def __call__(self, data: dict, expires_delta: timedelta | None = None):
        to_encode = data.copy()
        if expires_delta:
            expire = datetime.utcnow() + expires_delta
        else:
            expire = datetime.utcnow() + timedelta(minutes=15)
        to_encode.update({"exp": expire})
        encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
        return encoded_jwt


@app.post("/token", response_model=Token)
async def login_for_access_token(
    form_data: Annotated[OAuth2PasswordRequestForm, Depends()],
    token_func: Annotated[TokenCreate, Depends()],
    db: Session = Depends(get_db),
):
    user = authenticate_user(db, form_data.username, form_data.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = token_func(
        data={"sub": user.login}, expires_delta=access_token_expires
    )
    return {"access_token": access_token, "token_type": "bearer"}


async def get_current_user(
    token: Annotated[str, Depends(oauth2_scheme)], db: Session = Depends(get_db)
):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    expired_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Token expired",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        username: str = payload.get("sub")
        if username is None:
            raise credentials_exception
        token_data = TokenData(username=username)
    except ExpiredSignatureError:
        raise expired_exception
    except JWTError:
        raise credentials_exception
    user = get_employee(db, username)
    if user is None:
        raise credentials_exception
    return user


@app.get(
    "/position-info",
    response_model=PositionInfo,
    summary="Get current salary and next promotion date",
)
async def check_position_info(
    current_user: Annotated[Employee, Depends(get_current_user)],
    db: Session = Depends(get_db),
):
    return get_employee_position_info(db, current_user.id)


if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=8000)
