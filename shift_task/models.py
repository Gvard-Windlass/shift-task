from sqlalchemy import (
    Column,
    ForeignKey,
    Integer,
    String,
    Float,
    Date,
    UniqueConstraint,
)
from sqlalchemy.orm import relationship

from .db_config import Base


class Empolyee(Base):
    __tablename__ = "employees"

    id = Column(Integer, primary_key=True, index=True)
    login = Column(String(25), nullable=False)
    first_name = Column(String(100), nullable=False)
    last_name = Column(String(100), nullable=False)
    surname = Column(String(100), nullable=False)
    hashed_password = Column(String, nullable=False)

    promotions = relationship("Promotion", back_populates="employee")
    __table_args__ = (UniqueConstraint("login", name="employee_unique_login"),)


class Position(Base):
    __tablename__ = "positions"

    id = Column(Integer, primary_key=True, index=True)
    title = Column(String(100), nullable=False)
    salary = Column(Float, nullable=False)

    promotions = relationship("Promotion", back_populates="position")


class Promotion(Base):
    __tablename__ = "promotions"

    id = Column(Integer, primary_key=True, index=True)
    promotion_date = Column(Date, nullable=False)
    position_id = Column(Integer, ForeignKey("positions.id"))
    employee_id = Column(Integer, ForeignKey("employees.id"))

    position = relationship("Position", back_populates="promotions")
    employee = relationship("Empolyee", back_populates="promotions")
