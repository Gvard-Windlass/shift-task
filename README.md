# Shift Task

## Working with project
To set up and run this project, follow the instructions:
1. Clone this repo by runnig 

`git clone https://gitlab.com/Gvard-Windlass/shift-task.git`

2. Create virtual environment and install project dependencies using [poetry](https://python-poetry.org/):
   - `poetry shell`
   - `poetry install`

3. Create .env file in `shift_task` folder (where `main.py` is) with your PostgreSQL credentials:

```
POSTGRES_USER="your_username"
POSTGRES_PASSWORD="your_password"
```

4. Start postgres server: `pg_ctl start`
5. Connect to cluster with psql: `psql -U your_username`
6. Create project database: `CREATE DATABASE gvard_shift;`
7. Apply [alembic](https://alembic.sqlalchemy.org/en/latest/) migrations: `alembic upgrade heads`
8. Insert demo data by using the provided `insert.sql` file:
   - `\c gvard_shift`
   - `\i insert.sql`
9.  Run project: `uvicorn shift_task.main:app --reload`
10. You can interact with application by using API docs at http://127.0.0.1:8000/docs :
    - Click `Authorize` button at the top of the page and enter following credentials:
      - username: `john-doe`
      - password: `Bk7^31&3LDXt`
    - try out `/token` and `/position-info` endpoints

### Tests
You can run tests by using `pytest tests` command.

## Other projects
You can also check out my other projects on my [github](https://github.com/Gvard-Windlass), for example:

[Django backend](https://github.com/Gvard-Windlass/teahouse)

[Django Rest Framework API](https://github.com/Gvard-Windlass/foodinfo)