INSERT INTO
    employees (login, first_name, last_name, surname, hashed_password)
VALUES
    ('john-doe', 'John', 'Doe', 'Huntington', '$2b$12$.mBSEnGCy/VDkT/vKNg2R.yM82JdiOmWm4ej6coZE7L5NylgPGQ1W');

INSERT INTO
    positions (title, salary)
VALUES
    ('j. dev', 30000),
    ('m. dev', 75000),
    ('s. dev', 120000),
    ('l. dev', 200000);

INSERT INTO
    promotions (promotion_date, position_id, employee_id)
VALUES
    ('2022-06-01', 1, 1),
    ('2023-06-01', 2, 1),
    ('2025-06-01', 3, 1),
    ('2027-06-01', 4, 1);