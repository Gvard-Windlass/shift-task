from datetime import date, timedelta
from datetime import datetime, timedelta

from sqlalchemy.orm import Session
from fastapi.testclient import TestClient
from jose import jwt

from shift_task.models import Empolyee, Position, Promotion
from shift_task.main import (
    SECRET_KEY,
    ALGORITHM,
    app,
    TokenCreate,
)

from .fixtures import session, client
from .factories import (
    EmployeeFactory,
    PositionFactory,
    PromotionFactory,
    get_test_user_password,
)


def test_employee_model(session: Session):
    EmployeeFactory.set_session(session)
    EmployeeFactory.create_batch(3)
    session.commit()
    employees = session.query(Empolyee).all()
    assert len(employees) == 3


def test_position_model(session: Session):
    PositionFactory.set_session(session)
    PositionFactory.create_batch(3)
    session.commit()
    positions = session.query(Position).all()
    assert len(positions) == 3


def test_promotion_model(session: Session):
    EmployeeFactory.set_session(session)
    PositionFactory.set_session(session)
    PromotionFactory.set_session(session)

    PromotionFactory.create_batch(3)
    session.commit()
    promotions = session.query(Promotion).all()
    assert len(promotions) == 3


def test_correct_login(session: Session, client: TestClient):
    EmployeeFactory.set_session(session)
    EmployeeFactory.reset_sequence()
    user = EmployeeFactory()
    session.commit()
    password = get_test_user_password(user)

    assert len(session.query(Empolyee).all()) == 1

    response = client.post(
        "/token",
        data={"username": user.login, "password": password},
    )
    assert response.status_code == 200
    assert "access_token" in response.json()


def test_incorrect_login(session: Session, client: TestClient):
    EmployeeFactory.set_session(session)
    user = EmployeeFactory()
    session.commit()

    assert len(session.query(Empolyee).all()) == 1

    response = client.post(
        "/token",
        data={"username": user.login, "password": "123"},
    )
    assert response.status_code == 401


def test_info_endpoint_no_token(client: TestClient):
    response = client.get(
        "/position-info", headers={"Authorization": "Bearer no token"}
    )
    assert response.status_code == 401
    response_data: dict = response.json()
    assert response_data.get("detail", None) == "Could not validate credentials"


def test_info_endpoint_with_token(session: Session, client: TestClient):
    EmployeeFactory.set_session(session)
    PositionFactory.set_session(session)
    PromotionFactory.set_session(session)
    EmployeeFactory.reset_sequence()

    user = EmployeeFactory()
    positions: list[Position] = PositionFactory.create_batch(4)
    PromotionFactory.create(
        promotion_date=date(2023, 1, 1), position=positions[0], employee=user
    )
    PromotionFactory.create(
        promotion_date=date.today(), position=positions[1], employee=user
    )
    closest_promotion: Promotion = PromotionFactory.create(
        promotion_date=date.today() + timedelta(days=120),
        position=positions[2],
        employee=user,
    )
    PromotionFactory.create(
        promotion_date=date.today() + timedelta(days=360),
        position=positions[3],
        employee=user,
    )
    session.commit()

    token = (
        client.post(
            "/token",
            data={"username": user.login, "password": get_test_user_password(user)},
        )
        .json()
        .get("access_token", "")
    )

    response = client.get(
        "/position-info", headers={"Authorization": f"Bearer {token}"}
    )
    assert response.status_code == 200

    response_data: dict = response.json()
    assert response_data.get("salary", None) == positions[1].salary
    assert response_data.get("next_promotion_date") == str(
        closest_promotion.promotion_date
    )


def test_info_no_promotions(session: Session, client: TestClient):
    EmployeeFactory.set_session(session)
    PositionFactory.set_session(session)
    PromotionFactory.set_session(session)
    EmployeeFactory.reset_sequence()

    user = EmployeeFactory()
    positions: list[Position] = PositionFactory.create_batch(3)
    PromotionFactory.create(
        promotion_date=date.today(), position=positions[1], employee=user
    )
    session.commit()

    token = (
        client.post(
            "/token",
            data={"username": user.login, "password": get_test_user_password(user)},
        )
        .json()
        .get("access_token", "")
    )

    response = client.get(
        "/position-info", headers={"Authorization": f"Bearer {token}"}
    )
    assert response.status_code == 200

    response_data: dict = response.json()
    assert response_data.get("salary", None) == positions[1].salary
    assert response_data.get("next_promotion_date") == None


class TokenCreateExpired:
    def __call__(self, data: dict, expires_delta: timedelta | None = None):
        to_encode = data.copy()
        if expires_delta:
            expire = datetime.utcnow() - expires_delta
        else:
            expire = datetime.utcnow() - timedelta(minutes=15)
        to_encode.update({"exp": expire})
        encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
        return encoded_jwt


def test_expired_token(session: Session, client: TestClient):
    EmployeeFactory.set_session(session)
    EmployeeFactory.reset_sequence()
    app.dependency_overrides[TokenCreate] = TokenCreateExpired

    user = EmployeeFactory()
    session.commit()

    token = (
        client.post(
            "/token",
            data={"username": user.login, "password": get_test_user_password(user)},
        )
        .json()
        .get("access_token", "")
    )

    response = client.get(
        "/position-info", headers={"Authorization": f"Bearer {token}"}
    )
    assert response.status_code == 401
    response_data: dict = response.json()
    assert response_data.get("detail", None) == "Token expired"
