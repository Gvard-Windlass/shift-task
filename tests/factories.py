import factory
from datetime import date

from shift_task.models import Empolyee, Position, Promotion
from shift_task.utils import get_password_hash

test_password_base = "Bk7^31&3LDXt"


def get_test_user_password(user: Empolyee):
    return f"{test_password_base}{int(user.id)-1}"


class BaseFactory(factory.alchemy.SQLAlchemyModelFactory):
    @classmethod
    def set_session(cls, session):
        cls._meta.sqlalchemy_session = session


class EmployeeFactory(BaseFactory):
    class Meta:
        model = Empolyee

    login = factory.Sequence(lambda n: "Employee%d" % n)
    first_name = factory.Faker("first_name")
    last_name = factory.Faker("last_name")
    surname = factory.Faker("first_name")
    hashed_password = factory.Sequence(
        lambda n: get_password_hash(f"{test_password_base}{n}")
    )


class PositionFactory(BaseFactory):
    class Meta:
        model = Position

    title = factory.Faker("job")
    salary = factory.Faker("pyfloat", positive=True, min_value=20000, max_value=220000)


class PromotionFactory(BaseFactory):
    class Meta:
        model = Promotion

    promotion_date = factory.Faker("date_between", start_date=date(2020, 1, 1))
    position = factory.SubFactory(PositionFactory)
    employee = factory.SubFactory(EmployeeFactory)
